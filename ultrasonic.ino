//UJI ULTRASONIC

#define Trigger_pin  12
#define Echo_pin  11

long duration, distance;
void setup() {
  Serial.begin(9600);
  pinMode(Trigger_pin,OUTPUT);
  pinMode(Echo_pin, INPUT);
  
}

void loop() {
  digitalWrite(Trigger_pin, LOW);
  delayMicroseconds(2);
  digitalWrite(Trigger_pin, HIGH);
  delayMicroseconds(5);
  digitalWrite(Trigger_pin, LOW);
  duration = pulseIn(Echo_pin, HIGH);
  distance = duration * 0.034 / 2;
  if(distance== 0 || distance > 300){
    Serial.print("Out of The range");
  }
  Serial.print("Distance = ");
  Serial.print(distance);
  Serial.print(" cm");
}
